module.exports = {

    // FreqMap constructor
    FreqMap : function() {
        var map = {};

        // Public methods
        return {

            totalSize: null,

            // Adds a pixel to the frequency map
            addPixel: function(r, g, b) {
                var key = r + "," + g + "," + b,
                    obj = map[key];

                if(!obj) {
                    map[key] = { r: r, g: g, b: b, cnt: 1 }; // First of its kind
                } else {
                    obj.cnt ++; // Increment counter
                }
            },

            getMap: function() {
                return map;
            }

        };
    }
};