colorizerApp.TranslatorViewmodel = function() {
    return {
        translation: ko.observable({}),
        translate: function(key) { return this.translation()[key] || key; }
    };
};