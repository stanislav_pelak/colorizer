colorizerApp.GalleryItemViewmodel = function(name, request) {

    var model = {
        name: name,
        src: ko.observable(),
        uploading: ko.observable(false),
        processing: ko.observable(false),
        progress: ko.observable(0),
        request: request,
        error: ko.observable(),
        colors: ko.observable(),
        clearEvents: function() {
            this.uploading(false);
            this.processing(false);
            this.progress(0);
        },
        closeItem: function() {},
        onColorsLoaded: function() {}
    };

    // Call on colors loaded callback and unsubscribe
    var subscription = model.colors.subscribe(function(data) {
        if(typeof model.onColorsLoaded === "function") {
            model.onColorsLoaded(data);
        }
        subscription.dispose();
    });

    return model;
};