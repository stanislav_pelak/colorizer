// Shared model for all controllers
colorizerApp.MainViewmodel = function(translatorModel) {

    var model = {
        // Browser support
        uploadSupport: !!window.FormData,
        progressSupport: "upload" in new XMLHttpRequest,
        dragSupport: $("<div>").customDropUploader.isSupported(),
        thumbnailSupport: !!window.FileReader,

        // Application support
        supportedMimeTypes: ko.observableArray([]),

        // Shared properties
        newFiles: ko.observableArray([]),
        gallery: ko.observableArray([])
    };

    // Include the translation
    ko.utils.extend(model, translatorModel);

    return model;
};