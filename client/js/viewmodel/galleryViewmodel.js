colorizerApp.GalleryViewmodel = function(parentViewmodel, onShowPreview) {

    // Model
    var model = {

        // Show preview event handler
        showPreview: onShowPreview

    };

    // Override defaults with parent viewmodel
    if(!! parentViewmodel) {
        ko.utils.extend(model, parentViewmodel);
    }

    return model;
};