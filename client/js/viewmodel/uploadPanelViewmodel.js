colorizerApp.UploadPanelViewmodel = function(parentViewmodel) {

    // Model
    var model = {
    };

    // Override defaults with parent viewmodel
    if(!! parentViewmodel) {
        ko.utils.extend(model, parentViewmodel);
    }

    return model;
};