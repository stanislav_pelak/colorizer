// Global superobject
var colorizerApp = {};

// Application "entry point"
$(function() {

    // Init partials
    var initPartials = function(translatorModel, mainModel, messenger) {
        // Translate "static" views
        $("[data-viewmodel=translatorViewmodel]").each(function(i, container) {
            ko.applyBindings(translatorModel, container);
        });

        // Init partials
        colorizerApp.galleryManager.init($("[data-viewmodel=galleryViewmodel]"), mainModel);
        colorizerApp.uploadPanelManager.init($("[data-viewmodel=uploadPanelViewmodel]"), mainModel, colorizerApp.AsyncFileUploader(messenger));
    };

    // Make this function the last one to run on document ready
    setTimeout(function() {
        var translatorModel, mainModel, messenger, translationProvider, initMessenger, initDone;

        // Init Messenger for API calls
        initMessenger = function() {
            messenger = colorizerApp.Messenger(mainModel);

            // Retrieve general configuration (supported MimeTypes)
            messenger.getSupportedTypes(false, function(data) {
                mainModel.supportedMimeTypes(data);
                initDone();
            });
        };

        // Init the application after all the pre-init configuration
        initDone = _.after(2, function() { initPartials(translatorModel, mainModel, messenger); });

        // Main model
        translatorModel = colorizerApp.TranslatorViewmodel();
        mainModel = colorizerApp.MainViewmodel(translatorModel);

        // I18n
        translationProvider = colorizerApp.translationProvider.loadLanguage(window.navigator.userLanguage || window.navigator.language || "en", function(data) {
            mainModel.translation(data);
            initDone();

            // Init the messenger after the translation is loaded
            initMessenger();
        });

    }, 0);
});