colorizerApp.galleryManager = (function() {
    var model, onNewFilesAdded, onShowPreview, onCloseItem, onColorsLoaded, colorPreviewTpl;

    colorPreviewTpl = "<div><div class='colorPreviewTooltip'>
            <div class='preview'>
                <div class='colorPreviewPanel'/>
            </div>
            <div class='rgb'/>";

    onCloseItem = function(obj, e) {
        $(e.target).parents(".galleryItem").find(".colorTile").off(); // Remove handlers
        obj.request.abortUpload();
        model.gallery.remove(obj);
    };

    onColorsLoaded = function(colors) {
        setTimeout(function() {
            $(".galleryItem .colorTile:not(.tooltipized)").each(function(i, panel) {
                var r, g, b, c, tile, content;

                tile = $(panel);
                r = tile.attr("data-r");
                g = tile.attr("data-g");
                b = tile.attr("data-b");
                c = tile.attr("data-coverage");

                content = $(colorPreviewTpl);
                content.find(".rgb").html("RGB: ( " + r + ", " + g + ", " + b + " )");
                content.find(".colorPreviewPanel").css("background-color", "rgb(" + r + "," + g + "," + b + ")");

                if(c.trim() !== "" || !isNaN(c)) {
                    content.find(".colorPreviewPanel").html(parseFloat(c).toFixed(2) + " %");
                }

                // Initialize tooltip
                tile.attr("title", content.html());
                tile.addClass("tooltipized").tooltip({
                    placement: "bottom",
                    html: true
                });
            });
        }, 0);
    };

    onNewFilesAdded = function(files) {
        if(!files.length) { return; }
        model.newFiles([]); // Clear

        _.forEach(files, function(file) {

            // Add event handlers
            file.closeItem = onCloseItem;
            file.onColorsLoaded = onColorsLoaded;

            // Add to gallery
            model.gallery.push(file);
        });
    };

    // Show modal with the image preview
    onShowPreview = function(item, event) {
        var colorsData, img;

        colorsData = ko.unwrap(item.colors);

        body = $("<div class='imagePreview'><img src='" + ko.unwrap(item.src) + "' class='img-responsive'/>");
        BootstrapDialog.show({
            title: ko.unwrap(item.name),
            message: body
        });
        body = null;
    };

    return {
        init: function(container, mainModel) {

            // Init model
            model = colorizerApp.GalleryViewmodel(mainModel, onShowPreview);

            // Init ko bindings
            ko.applyBindings(model, container[0]);

            // Subscribe
            model.newFiles.subscribe(onNewFilesAdded);
        }
    };

} ());