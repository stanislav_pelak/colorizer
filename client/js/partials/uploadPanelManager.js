colorizerApp.uploadPanelManager = (function() {

    // ----------------------- Private properties ------------------------

    var container, model, filesAddedHandler, filesRejectedHandler, initUploadDropPanel, uploader;

    // ------------------------- Private methods -------------------------

    // Reads the provided image file. Callback is called when finished with
    // encoded file as an argument.
    createThumbnail = function(file, callback) {
        var reader = new FileReader();
        reader.onload = function(event) {
            callback(event.target.result);
        }
        reader.readAsDataURL(file);
    };

    // Files accepted for upload
    filesAddedHandler = function(files) {
        var i, file, request;

        for(i = 0; file = files[i]; i++) {

            // Create new knockout object and add it to the gallery
            model.newFiles.push((function() {
                var koFile, request;

                request = uploader.buildRequest();
                koFile = colorizerApp.GalleryItemViewmodel(file.name, request);

                createThumbnail(file, function(src) { koFile.src(src); });

                // Initialize the upload request
                request.onLoadStart(function() { koFile.uploading(true); });
                request.onPogress(function(e) { koFile.progress((e.total === 0)? 100 : parseInt((e.loaded / e.total) * 100)); });
                request.onLoad(function() { koFile.processing(true); koFile.uploading(false); });
                request.onSuccess(function(data, status, request) {
                    if(data.length > 5) {
                        data = data.splice(0, 5);
                    }
                    data = _.map(data, function(color) {
                        color.r = Math.round(color.r);
                        color.g = Math.round(color.g);
                        color.b = Math.round(color.b);
                        return color;
                    });
                    koFile.colors(data);
                    koFile.clearEvents();
                });
                request.onError(function(req, status, error) {
                    var response, status;

                    response = (req.responseText || "(no response)").replace("\n", "");
                    status = req.status;

                    koFile.clearEvents();
                    koFile.error(model.translate("ProcessingRequestFailed"));
                    console.error("Request failed: " + response + " - " + status + " " + error);
                });

                // Trigger upload
                request.startUpload(file);
                return koFile;
            } ()));
        }
    };

    // Files rejected for upload
    filesRejectedHandler = function(files) {
        var body, list;

        body = $("<div><div class='message'/><ul/><div class='supported'>");
        body.find(".message").html(model.translate("RejectedFilesMessage"));
        body.find(".supported").html(model.translate("SupportedTypes") + ": [" + ko.unwrap(model.supportedMimeTypes) + "].");

        list = body.find("ul");

        files.forEach(function(file) {
            list.append("<li>" + file.name + " (" + file.type + ")</li>");
        });

        BootstrapDialog.alert({
            type: BootstrapDialog.TYPE_WARNING,
            title: model.translate("RejectedFiles"),
            message: body
        });
        body = null;
    };

    // Initialize the drop/prompt upload area
    initUploadDropPanel = function() {
        var settings, uploaderPanel;

        settings = {
            types: ko.unwrap(model.supportedMimeTypes),

            // File(s) added handlers
            onDrop: filesAddedHandler,      // for Drop
            onSelect: filesAddedHandler,    // for Prompt

            // File(s) rejected handler
            onReject: filesRejectedHandler
        };

        // Init the area
        uploaderPanel = container.find(".customUploader.uploadPanel");
        if(model.dragSupport) {
            uploaderPanel.customDropUploader(settings);
        }
        uploaderPanel.customUploadPrompt(settings);
    };

    // ------------------------- Manager's public API -------------------------

    return {

        /**
         * Initialize the upload panel partial
         *
         * @param uploaderContainer - jQuery object referencing the upload panel container
         * @param mainModel - "super" model to inherit
         * @param fileUploader - object managing file uploads
         */
        init: function(uploaderContainer, mainModel, fileUploader) {
            if(!!model) { throw "The partial has already been initialized"; }

            // Set private properties
            model = colorizerApp.UploadPanelViewmodel(mainModel);
            container = uploaderContainer;
            uploader = fileUploader;

            // Init ko bindings
            ko.applyBindings(model, container[0]);

            // Tear down if not supported
            if(!model.uploadSupport) { return; }

            initUploadDropPanel();
        }
    };

} ());