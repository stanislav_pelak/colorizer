/**
 * Factory (Power Constructor) for a Messenger object managing the API calls
 *
 * @param mainModel - super model
 * @param customAPI - (optional) overrides default API configuration
 * @return A new Messenger object
 */
colorizerApp.Messenger = function(mainModel, customAPI) {

    // Default API configuration
    var api = {

        // Supported MimeTypes for upload
        mimetypes: {
            url: "/api/types",
            type: "GET"
        },

        process: {
            url: "/api/process",
            type: "POST",
            headers: { "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" },
            processData: false,
            cache: false,
            contentType: false
        }
    };

    // Extend/Override by the custom API configuration
    $.extend(api, customAPI);

    // Return the messenger
    return {

        // General method for calling any custom API
        callAPI: function(apiName, blocking, successCallback, errorCallback) {
            var options = $.extend(api[apiName], { success: successCallback, error: errorCallback });
            if(blocking) {
                $.blockingAjax(mainModel.translate("Loading"), options);
            } else {
                $.ajax(options);
            }
        },

        // Get supported MimeTypes API
        getSupportedTypes: function(blocking, successCallback, errorCallback) {
            this.callAPI("mimetypes", blocking, successCallback, errorCallback);
        },

        // Call Process API
        processFile: function(blocking, ajaxOptions) {
            var options = $.extend(api.process, ajaxOptions);
            if(blocking) {
                return $.blockingAjax(mainModel.translate("Loading"), options);
            } else {
                return $.ajax(options);
            }
        }
    };
};