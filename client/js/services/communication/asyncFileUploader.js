// AsyncFileUploader constructor
colorizerApp.AsyncFileUploader = function(messenger) {

	var defaultXhrOptions, defaultAjaxOptions, callbacksPrototype;

	// Default options
	defaultXhrOptions = {
		upload: {
			onloadstart	: function() {},
			onloadend	: function() {},
			onload 		: function() {},
			onprogress	: function() {},
			onabort		: function() {},
			ontimeout	: function() {}
		}
	};

	defaultAjaxOptions = {
		xhr: function() {},
	    beforeSend: function() {},
	  	success: function() {},
		error: function() {},
		complete: function() {}
	};

	// Return a new Uploader object with its public API
	return {

		// Builds a request object
		buildRequest: function() {
			var xhrOptions, ajaxOptions, request;

			if(!window.FormData) {
				console.error("FormData is not supported by the browser - can't upload.");
				return;
			};

			// Clone default configuration
			xhrOptions = $.extend({}, defaultXhrOptions);
			ajaxOptions = $.extend({}, defaultAjaxOptions);
			ajaxOptions.complete = function() { request = null; };

			// Return a new request object with its public API
			return {

				// XHR callbacks
				onLoad 		: function(callback) { xhrOptions.upload.onload 		= callback; },
				onLoadStart	: function(callback) { xhrOptions.upload.onloadstart	= callback; },
				onLoadEnd	: function(callback) { xhrOptions.upload.onloadend		= callback; },
				onPogress 	: function(callback) { xhrOptions.upload.onprogress 	= callback;	},
				onAbort 	: function(callback) { xhrOptions.upload.onabort 		= callback; },
				onTimeout 	: function(callback) { xhrOptions.upload.ontimeout		= callback; },

				// AJAX callbacks
				beforeSend 	: function(callback) { ajaxOptions.beforeSend 	= callback; },
				onError 	: function(callback) { ajaxOptions.error 		= callback; },
				onSuccess 	: function(callback) { ajaxOptions.success		= callback; },

				// Abort upload
				abortUpload : function() {
					request && request.abort();
				},

				// Start upload
				startUpload : function(files) {
					files = files || [];

					// Convert to array if needed
					if(files.length === undefined) { files = [files]; }

					// Data
					var formData = new FormData();
					for (var i = 0, file; file = files[i]; i++) {
						formData.append('files' + i, file);
					}
					ajaxOptions.data = formData;

					// Prepare XHR request
					var xhr = new XMLHttpRequest();
					$.extend(xhr.upload, xhrOptions.upload);
					ajaxOptions.xhr = function() { return xhr; };

					// Send AJAX request
					request = messenger.processFile(false, ajaxOptions);
				}
			};
		}

	};
};