colorizerApp.translationProvider = (function() {

    // Public Translation Provider API
    return {

        // Loads a dictionary for the specified language or a default one
        loadLanguage: function(language, callback) {
            language = language || "en"; // Default language

            var successCallback = function(data) {
                if(typeof callback === "function") {
                    Object.freeze(data);
                    callback(data);
                }
            }

            // Validate language and extract the first part (en-GB -> en)
            if(!/^[a-zA-Z]{2,3}(-[a-zA-Z]{2,3})?$/.test(language)) {
                throw "Invalid language: " + language;
            }
            language = language.match("^[a-z]{2,3}");

            // Retrieve language
            $.get("lang/" + language + ".json", successCallback).fail(function() {
                $.get("lang/en.json", successCallback);
            });
        }
    };

} ());
