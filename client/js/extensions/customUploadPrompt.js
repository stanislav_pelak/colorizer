/**
 * Custom HTML5-based Drag&drop uploader.
 *
 * Reusing Han Lin's AjaxUploader.
 */
$(function() {
	var isSupportedType, handleFileSelected;

	// Utils

	isSupportedType = function(type, supportedTypes) {
		var genericType;

		// Not restricted
		if(supportedTypes == null || !supportedTypes.length) {
			return true;
		}

		// Type (a/b) in array
		if($.inArray(type, supportedTypes) >= 0) {
			return true;
		}

		// Type (a/*) allowed
		genericType = type.substr(0, type.indexOf("/") + 1) + "*";
		if($.inArray(genericType, supportedTypes) >= 0) {
			return true;
		}

		// Any type (*/*) allowed
		if($.inArray("*/*", supportedTypes) >= 0) {
			return true;
		}

		return false;
	};

	/**
	 * Handle file selected
	 *
	 * @param container - parent container
	 * @param files
	 * @param supportedTypes - String array of supported types (e.g. text/xml, image/*, ...)
	 */
	handleFileSelected = function(container, files, supportedTypes) {
		var loadedFiles = [], rejectedFiles = [], i, file, filesDropped, filesRejectedEvent;

		for (i = 0; file = files[i]; i++) {
			// Read the File objects in this FileList
			if(isSupportedType(file.type, supportedTypes)) {
				loadedFiles.push(file);
			} else {
				file.invalid = ["type"];
				rejectedFiles.push(file);
			}
		}

		// Trigger files dropped event
		if(loadedFiles.length > 0) {
			filesDropped = $.Event("filesSelected");
			filesDropped.files = loadedFiles;
			container.trigger(filesDropped);
		}

		// Trigger files rejected event
		if(rejectedFiles.length > 0) {
			filesRejectedEvent = $.Event("filesRejected");
			filesRejectedEvent.files = rejectedFiles;
			container.trigger(filesRejectedEvent);
		}

		// Reset input value
		$("input[data-uploadprompt=" + container.attr("data-uploadprompt") + "]").val(null);

		return {
			"loaded": loadedFiles,
			"rejected": rejectedFiles
		};
	};

	/**
	 * customUploadPrompt initialization
	 *
	 * @param options:
	 * 		types - String array
	 * 		onSelect - files selected callback
	 * 		onReject - files rejected callback
	 */
	$.fn.customUploadPrompt = function customUploadPrompt(options) {
		var form, input, container = this, id;

		options = options || {};
		id = options.id || "main";

		// Set styles
		container.addClass("customUploader uploadPrompt");
		container.attr("data-uploadprompt", id);

		// Init file input
		this.find(".uploadPromptInput[data-uploadprompt=" + id + "]").off().remove();	// Clear relicts
		form = $('<form enctype="multipart/form-data"><input class="uploadPromptInput" data-uploadprompt="' + id + '" type="file" multiple>').hide().appendTo("body");
		input = form.find("input");

		// Init click listener
		container.click(function(e) {
			input.click();
		});

		input.on("change", function(e) {
			var files, result, loaded, rejected;

			files = this.files;
			if(!files.length) {
				return;
			}

			result = handleFileSelected(container, files, options.types);
			loaded = result.loaded;
			rejected = result.rejected;

			// Call callbacks
			if(typeof options.onSelect === "function") {
				options.onSelect(loaded);
			}
			if(typeof options.onReject === "function" && !!rejected.length) {
				options.onReject(rejected);
			}
		});

		return this;
	};

});
