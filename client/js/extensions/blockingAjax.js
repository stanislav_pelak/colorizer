$(function() {

    // Init modal
    var modalTemplate = $("<div class='modal processingModal'><div class='modal-dialog'><div class='modal-body label label-info'><span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> <span class='caption'></span></div></div></div>");
    modalTemplate.hide();
    modalTemplate.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });
    $("body").append($(modalTemplate));

    // Show modal
    var showModal = function(caption) {
        hideModal();
        $(".processingModal .caption").html(caption);
        $(".processingModal").modal("show");
    };

    // Hide modal
    var hideModal = function() {
        $(".processingModal").modal("hide");
    };

    /**
     * Method decorating AJAX request with a (modal) processing indicator.
     * Modal is displayed before sending a request and hidden when a response is retrieved.
     * Method doesn't affect regular success/error callbacks.
     *
     * @param caption - caption of the modal
     * @param options - regular jQuery.ajax options
     */
    $.blockingAjax = function blockingAjax(caption, options) {

        // Keep original callbacks
        var __success = options.success;
        var __error = options.error;

        // Custom success callback - hide blocker, call original callback
        options.success = function(data, status, request) {
            hideModal();
            if(typeof __success === "function") {
                __success.call(this, data, status, request);
            }
        };

        // Custom error callback - hide blocker, call original callback
        options.error = function(request, status, error) {
            hideModal();
            if(typeof __error === "function") {
                __error.call(this, request, status, error);
            }
        };

        // Show modal and send AJAX request
        showModal(caption);
        return $.ajax(options);
    };
});