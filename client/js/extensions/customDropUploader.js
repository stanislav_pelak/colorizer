/**
 * Custom HTML5-based Drag&drop uploader.
 *
 * Reusing Han Lin's AjaxUploader.
 */
$(function() {
	var notifyUnsupported, isSupported, isSupportedType, handleDragOver, handleDrop;

	// Utils

	notifyUnsupported = function(name, supported) {
		if (!supported) {
			console.err(name + " is not supported");
		}
		return supported;
	};

	isSupported = function() {
		if(!notifyUnsupported("Drag & Drop", "draggable" in document.createElement('span'))) {
			return false;
		}
		if(!notifyUnsupported("FormData", !!window.FormData)) {
			return false;
		};
		return true;
	};

	isSupportedType = function(type, supportedTypes) {
		var genericType;

		// Not restricted
		if(supportedTypes == null || !supportedTypes.length) {
			return true;
		}

		// Type (a/b) in array
		if($.inArray(type, supportedTypes) >= 0) {
			return true;
		}

		// Type (a/*) allowed
		genericType = type.substr(0, type.indexOf("/") + 1) + "*";
		if($.inArray(genericType, supportedTypes) >= 0) {
			return true;
		}

		// Any type (*/*) allowed
		if($.inArray("*/*", supportedTypes) >= 0) {
			return true;
		}

		return false;
	};

	handleDragOver = function(e) {
		e.stopPropagation(); e.preventDefault();
		e.dataTransfer.dropEffect = 'copy';
	};

	/**
	 * Handle drop event
	 * @param e - event
	 * @param supportedTypes - String array of supported types (e.g. text/xml, image/*, ...)
	 */
	handleDrop = function(e, supportedTypes) {
		var target, files, i, file, filesDropped, filesRejectedEvent;

		// Stop some browsers from redirecting.
		e.stopPropagation(); e.preventDefault();

		target = $(e.target);
		target.removeClass("dragover");

		files = e.dataTransfer.files,
			loadedFiles = [],
			rejectedFiles = [];

		// Validate
		for (i = 0; file = files[i]; i++) {
			// Read the File objects in this FileList
			if(isSupportedType(file.type, supportedTypes)) {
				loadedFiles.push(file);
			} else {
				file.invalid = ["type"];
				rejectedFiles.push(file);
			}
		}

		// Trigger files dropped event
		if(loadedFiles.length > 0) {
			filesDropped = $.Event("filesDropped");
			filesDropped.files = loadedFiles;
			target.trigger(filesDropped);
		}

		// Trigger files rejected event
		if(rejectedFiles.length > 0) {
			filesRejectedEvent = $.Event("filesRejected");
			filesRejectedEvent.files = rejectedFiles;
			target.trigger(filesRejectedEvent);
		}

		return {
			"loaded": loadedFiles,
			"rejected": rejectedFiles
		};
	};

	/**
	 * customDropUploader initialization
	 *
	 * @param options:
	 * 		types - String array
	 * 		onDrop - files dropped callback
	 * 		onReject - files rejected callback
	 */
	$.fn.customDropUploader = function customDropUploader(options) {
		var container = this;

		options = options || {};

		// Restore original drop event
		$.event.special.drop = undefined;

		container.addClass("customUploader");

		// Check support
		if(!isSupported()) { return null; }

		// Drag styles
		container.addClass("customDragUploader");
		container.on("dragenter", function() { container.addClass("dragover"); });
		container.on("dragleave", function() { container.removeClass("dragover"); });

		// Drop event - read file
		container[0].addEventListener("dragover", handleDragOver, false);
		container[0].addEventListener("drop", function(e) {

			// Add to Form Data object
			var result = handleDrop(e, options.types),
				loaded = result.loaded,
				rejected = result.rejected;

			// Call callbacks
			if(typeof options.onDrop === "function") {
				options.onDrop(loaded);
			}
			if(typeof options.onReject === "function" && !!rejected.length) {
				options.onReject(rejected);
			}
		}, false);

		return this;
	};

	// Make the isSupported method public
	$.fn.customDropUploader.isSupported = isSupported;

});
