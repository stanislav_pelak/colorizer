var chai = require("chai"),
    expect = chai.expect,
    assert = chai.assert,

    colorUtils = require("../../../business/utils/colorUtils");

describe("Color utils test", function() {

    it("should sort colors", function() {
        var colorsArr, colorsObj, expected;

        // Prepare
        colorsArr = [
            {cnt: 10},
            {cnt: 2},
            {cnt: 160},
            {cnt: 54},
            {cnt: 0}
        ];

        colorsObj = {
            "1": {cnt: 10},
            "test": {cnt: 2},
            "something": {cnt: 160},
            "key": {cnt: 54},
            "asd": {cnt: 0},
        };

        expected = [
            {cnt: 160},
            {cnt: 54},
            {cnt: 10},
            {cnt: 2},
            {cnt: 0}
        ];

        // Check
        expect(colorUtils.sortColors(colorsArr)).to.deep.equal(expected);
        expect(colorUtils.sortColors(colorsObj)).to.deep.equal(expected);

        // Not modified
        assert.deepEqual(colorsArr, [
            {cnt: 10},
            {cnt: 2},
            {cnt: 160},
            {cnt: 54},
            {cnt: 0}
        ]);
        expect(colorUtils.sortColors).not.to.change(colorsObj, "asd");
    });

    it("should merge colors", function() {
        var color1, color2, expected;

        // Prepare
        color1 = {
            cnt: 10,
            a: 1,
            coverage: 160,
            b: 5,
            something_else: 120
        };

        color2 = {
            coverage: 20,
            something_else: 0,
            a: 8,
            b: 12,
            cnt: 90
        };

        expected = {
            // Add
            cnt: 100,
            coverage: 180,

            // Weighted mean (using cnt as weight)
            a: 7.3, // 1*10 + 8*90 / 100
            b: 11.3, // 5*10 + 12*90 / 100
            something_else: 12 // 120*10 + 0*90 / 100
        };

        // Check
        expect(colorUtils.mergeColors(color1, color2)).to.deep.equal(expected);
        expect(colorUtils.mergeColors(color2, color1)).to.deep.equal(expected);

        // Not modified
        assert.deepEqual(color1, {
            cnt: 10,
            a: 1,
            coverage: 160,
            b: 5,
            something_else: 120
        });
        assert.deepEqual(color2, {
            coverage: 20,
            something_else: 0,
            a: 8,
            b: 12,
            cnt: 90
        });
    });

    it("should merge color with undefined", function() {
        var color;

        // Prepare
        color = {
            cnt: 10,
            a: 1,
            coverage: 160,
            b: 5,
            something_else: 120
        };

        // Check
        expect(colorUtils.mergeColors(null, color)).to.deep.equal(color);
        expect(colorUtils.mergeColors(color, undefined)).to.deep.equal(color);
    });

    it("should filter out noise", function() {
        var colorsArr, colorsObj, expected;

        // Prepare
        colorsArr = [
            {cnt: 16},
            {cnt: 5},
            {cnt: 61},
            {cnt: 15},
            {no_cnt_property: 14},
            {cnt: 49},
            {cnt: 0}
        ];

        colorsObj = {
            "a": {cnt: 16},
            "asd": {cnt: 5},
            "kjh": {cnt: 61},
            "c": {cnt: 15},
            "b": {cnt: 49},
            "c1": {no_cnt_property: 14},
            "d": {cnt: 0}
        };

        expected = [
            {cnt: 16, coverage: 10},
            {cnt: 61, coverage: 38.125},
            {cnt: 49, coverage: 30.625}
        ];

        // Check
        expect(colorUtils.filterNoise(colorsArr, 160, 10)).to.deep.equal(expected);
        expect(colorUtils.filterNoise(colorsObj, 160, 10)).to.deep.equal(expected);

        assert.deepEqual(colorsObj, {
            "a": {cnt: 16, coverage: 10},
            "asd": {cnt: 5, coverage: 3.125},
            "kjh": {cnt: 61, coverage: 38.125},
            "c": {cnt: 15, coverage: 9.375},
            "b": {cnt: 49, coverage: 30.625},
            "c1": {no_cnt_property: 14, coverage: 0},
            "d": {cnt: 0, coverage: 0}
        });

        assert.deepEqual(colorsArr, [
            {cnt: 16, coverage: 10},
            {cnt: 5, coverage: 3.125},
            {cnt: 61, coverage: 38.125},
            {cnt: 15, coverage: 9.375},
            {no_cnt_property: 14, coverage: 0},
            {cnt: 49, coverage: 30.625},
            {cnt: 0, coverage: 0}
        ]);
    });
});