var _ = require("lodash"),
    chai = require("chai"),
    expect = chai.expect,
    comparators = require("../../../../business/analyzer/comparators");

describe("Comparator", function() {

    // Check all comparators
    _.forEach(comparators, function(comparator, key) {

        // Test data
        var testColorsObj = {
            "a": {cnt: 10, r: 100, g: 150, b: 200 },
            "b": {cnt: 210, r: 20, g: 94, b: 243 },
            "c": {cnt: 170, r: 55, g: 105, b: 12 }
        };
        var testColorsArr = [
            {cnt: 10, r: 100, g: 150, b: 200 },
            {cnt: 210, r: 20, g: 94, b: 243 },
            {cnt: 170, r: 55, g: 105, b: 12 }
        ];

        // Pre-process
        it(key + " should provide the universal pre-process API", function() {
            var preProcess = comparator.preProcess(testColorsObj);
            expect(preProcess).to.be.a("array");
            expect(preProcess).to.have.length(3);

            var preProcessArr = comparator.preProcess(testColorsArr);
            expect(preProcessArr).to.be.a("array");
            expect(preProcessArr).to.have.length(3);

            expect(preProcess).to.deep.equal(preProcessArr);
        });

        // Distance
        it(key + " should provide a distance method", function() {
            var preProcess = comparator.preProcess(testColorsArr);

            // Provides at least one distance method
            expect(comparator.distance).to.not.be.empty;

            // Test all distance methods
            _.forEach(comparator.distance, function(distance) {

                // Distance === 0 for the same colours
                expect(distance(preProcess[0], preProcess[0])).to.equal(0);

                // Distance !== 0 for different colours
                expect(distance(preProcess[0], preProcess[1])).to.not.equal(0);
            });
        });

    });

});



