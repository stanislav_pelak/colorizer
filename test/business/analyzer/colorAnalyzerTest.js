var chai = require("chai"),
    expect = chai.expect,
    proxyquire = require("proxyquire"),

    colorAnalyzer;

describe("Color analyzer test", function() {

    // Stub configuration
    before(function() {
        var stubbedConfig = {
            colorAnalysis: {
                env: "test"
            }
        };
        colorAnalyzer = proxyquire('../../../business/analyzer/colorAnalyzer', { '../../config/config': stubbedConfig });
    });

    // Merge similar colors
    it("should merge similar colors", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 12,  a: 1 },   // 1
            { cnt: 23,  a: 10 },  // 2
            { cnt: 5,   a: 2 },   // 1
            { cnt: 6,   a: 100 }, // 3
            { cnt: 119, a: 11 },  // 2
            { cnt: 18,  a: 12 },  // 2
            { cnt: 12,  a: 3 }    // 1
        ];

        expected = [
            {cnt: 29,   a: 2, coverage: NaN}, // 1
            {cnt: 160,  a: 10.96875, coverage: NaN}, // 2
            {cnt: 6,    a: 100} // 3
        ];

        distance = function(a, b) {
            return Math.abs(a.a - b.a);
        };

        // same color to distance 0, different from distance 5, min 3 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 0, 5, 3);

        expect(actual).to.deep.equal(expected);
    });

    it("should return at least minimum number of colours (1)", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 11 },
            { cnt: 10,  a: 12 }
        ];

        expected = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 11 },
            { cnt: 10,  a: 12 }
        ];

        distance = function(a, b) {
            return Math.abs(a.a - b.a);
        };

        // same color to distance 0, different from distance 5, min 3 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 0, 5, 3);

        expect(actual).to.deep.equal(expected);
    });

    it("should return at least minimum number of colours (2)", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 12 },
            { cnt: 10,  a: 14 }
        ];

        expected = [
            { cnt: 20,  a: 11, coverage: NaN },
            { cnt: 10,  a: 14 }
        ];

        distance = function(a, b) {
            return Math.abs(a.a - b.a);
        };

        // same color to distance 0, different from distance 5, min 2 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 0, 5, 2);

        expect(actual).to.deep.equal(expected);
    });

    it("should always merge colours that are considered same", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 12 },
            { cnt: 10,  a: 14 }
        ];

        expected = [
            { cnt: 30,  a: 12, coverage: NaN }
        ];

        distance = function(a, b) {
            return 0; // All colors are same
        };

        // same color to distance 0, different from distance 5, min 2 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 0, 5, 2);

        expect(actual).to.deep.equal(expected);
    });

    it("should always merge colours that are considered same (with tolerance)", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 12 },
            { cnt: 10,  a: 14 }
        ];

        expected = [
            { cnt: 30,  a: 12, coverage: NaN }
        ];

        distance = function(a, b) {
            return Math.abs(a.a - b.a);
        };

        // same color to distance 3, different from distance 5, min 2 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 3, 5, 2);

        expect(actual).to.deep.equal(expected);
    });

    it("shouldn't merge colours that are considered different", function() {
        var colors, expected, distance, actual;

        // Prepare
        colors = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 12 },
            { cnt: 10,  a: 14 }
        ];

        expected = [
            { cnt: 10,  a: 10 },
            { cnt: 10,  a: 12 },
            { cnt: 10,  a: 14 }
        ];

        distance = function(a, b) {
            return Math.abs(a.a - b.a);
        };

        // same color to distance 0, different from distance 1, min 2 colors in the result set if possible
        actual = colorAnalyzer.mergeSimilarColors(colors, distance, 0, 1, 2);

        expect(actual).to.deep.equal(expected);
    });
});



