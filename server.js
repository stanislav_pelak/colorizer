var express = require("express"),
    bodyParser = require("body-parser"),
    multer = require("multer"),
    config = require("./config/config").server;

var app, port;

// Express
app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Routes
app.use("/", express.static("client"));
app.use("/api", require("./routes/api"));

// Start
port = (config && config.port) || 3000;
app.listen(port);
console.log("Server listening on port " + port);