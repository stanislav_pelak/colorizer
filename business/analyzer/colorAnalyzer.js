// Dependencies
var _ = require("lodash"),
    comparators = require("./comparators"),
    colorUtils = require("../utils/colorUtils");

// ----------------- Configuration -----------------

var defaultConfig = {
    minCoverage: 0.4, // minimum share of a color in the image (%)
    distance: "sum", // euclidean / sum
    maxDistanceToMerge: 0,
    minDistance: 0,
    colorSpace: "rgb" // rgb / yuv / cie
},
config =  _.merge(defaultConfig, require("../../config/config").colorAnalysis); // Import configuration

// ----------------- Export -----------------
module.exports = (function() {
    var mergeSimilarColors, toExport;

    // ----------------- Private methods ----------------

    /**
     * Processes the array of colours:
     * - Colours that are "too" similar are always merged.
     * - Tries to merge the most similar colours in each cycle.
     * - Ends if there are no colours to merge or the minimum size of the result set has been reached.
     *
     * @param colors - array of colors to process
     * @param calculateDistance - function for calculating the distance between two colours
     * @param minDistance - minimum distance for the colors to be considered different
     * @param maxMergeableDistance - maximum distance for the colors to be mergeable
     * @param minResultSetSize - minimum size of the result set (if possible)
     *
     * @return array of processed (merged) colours
     */
    mergeSimilarColors = function(colors, calculateDistance, minDistance, maxMergeableDistance, minResultSetSize) {
        var mergeResult, color, i, closestColor, distance,
            merged = true;

        maxMergeableDistance = maxMergeableDistance || 0;
        minDistance = minDistance || 0;
        minResultSetSize = minResultSetSize || 0;

        while(merged) { // While there still might be something to merge - start a new cycle
            mergeResult = [];
            merged = false;

            // Take a reference color
            while(color = colors.shift()) {

                // If there's nothing left to compare with - add to result and end the cycle
                if(!colors.length) {
                    mergeResult.push(color);
                    break;
                }

                // Find the most similar color
                closestColor = _.min(colors, function(tmpColor) { return calculateDistance(color, tmpColor); });
                distance = calculateDistance(color, closestColor);

                // ----------------------- Merge colors -----------------------

                // Merge if they're too similar, OR
                if ((distance <= minDistance) ||
                    // There's still enough colors AND the two are similar enough
                    ((mergeResult.length + colors.length >= minResultSetSize) && (distance <= maxMergeableDistance)))
                {

                    merged = true; // Set flag for next cycle
                    mergeResult.push(colorUtils.mergeColors(color, closestColor));
                    _.remove(colors, function(tmp) { return tmp === closestColor; }); // Remove the other color

                } else {
                    mergeResult.push(color); // Don't merge
                }
            }
            colors = mergeResult;
        }

        return mergeResult;
    };

    // ------------------- Public API ------------------

    toExport = {

        /**
         * Analyzes the colors frequency map and selects dominant colors.
         * @param freqMap - "instance" of FreqMap
         * @param minColorsCnt - minimum returned colors
         */
        getDominantColors: function(freqMap, minColorsCnt) {
            var colors = freqMap.getMap(),
                comparator = comparators[config.colorSpace];

            // Pre-process the frequencies
            colors = colorUtils.filterNoise(colors, freqMap.totalSize, config.minCoverage); // -> Array
            colors = comparator.preProcess(colors);

            // Do the magic
            colors = mergeSimilarColors(
                colors,
                comparator.distance[config.distance],   // distance function
                config.minDistance,                     // minimum distance for the colors to be considered different
                config.maxDistanceToMerge,              // maximum distance for the colors to be mergeable
                minColorsCnt || config.minResultSetSize // minimum size of the result set (if possible)
            );

            return colorUtils.sortColors(colors);
        }
    };

    // Publish public methods for testing
    if(config.env === "test") {
        toExport.mergeSimilarColors = mergeSimilarColors;
    }

    return toExport;
} ());