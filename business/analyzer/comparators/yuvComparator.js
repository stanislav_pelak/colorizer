// YUV color space comparator
// Recommended configuration maxDistanceToMerge: 30, minDistance: 5, weights: { y: 0.5 }

// Dependencies
var _ = require("lodash");

// Configuration
var defaultConfig = {
    maxDistanceToMerge: 10, // merge with similar color if their distance is less than this value (average)
    distance: "euclidean", // euclidean / sum
    weights: {
        y: 1
    }
},
config =  _.merge(defaultConfig, require("../../../config/config").colorAnalysis); // Import configuration

// Export
module.exports = (function() {

    // ------------------- Public API ------------------
    return {

        // Prepare the data for processing by this comparator
        preProcess: function(freqMap) {
            return _.map(freqMap, function(color) {
                // BT.709
                color.y = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
                color.u = -0.09991 * color.r - 0.33609 * color.g + 0.436 * color.b;
                color.v = 0.615 * color.r - 0.55861 * color.g - 0.05639 * color.b;
                return color;
            });
        },

        distance: {
            sum: function(color1, color2) {
                return Math.abs(color1.y - color2.y) * config.weights.y + Math.abs(color1.u - color2.u) + Math.abs(color1.v - color2.v);
            },

            euclidean: function(color1, color2) {
                return Math.sqrt(Math.pow(color1.y - color2.y, 2) * config.weights.y + Math.pow(color1.u - color2.u, 2) + Math.pow(color1.v - color2.v, 2));
            }
        }
    };
} ());