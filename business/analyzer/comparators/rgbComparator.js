// RGB color space comparator
// Recommended configuration: maxDistanceToMerge: 50, minDistance: 5

// Dependencies
var _ = require("lodash");

// Configuration
var defaultConfig = {
    maxDistanceToMerge: 10 // merge with similar color if their distance is less than this value (average)
},
config =  _.merge(defaultConfig, require("../../../config/config").colorAnalysis); // Import configuration

// Export
module.exports = (function() {

    // ------------------- Public API ------------------
    return {
        preProcess: function(freqMap, totalSize) {
            // Convert to array
            return _.map(freqMap, function(c) { return c; });
        },

        distance: {
            sum: function(color1, color2) {
                return Math.abs(color1.r - color2.r) + Math.abs(color1.g - color2.g) + Math.abs(color1.b - color2.b);
            },

            euclidean: function(color1, color2) {
                return Math.sqrt(Math.pow(color1.r - color2.r, 2) + Math.pow(color1.g - color2.g, 2) + Math.pow(color1.b - color2.b, 2));
            }
        }
    };
} ());