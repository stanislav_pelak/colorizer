// CIE 1931 color space comparator
// Recommended configuration: maxDistanceToMerge: 50, minDistance: 5

// Dependencies
var _ = require("lodash");

// Configuration
var defaultConfig = {
    maxDistanceToMerge: 10, // merge with similar color if their distance is less than this value (average)
    distance: "euclidean", // euclidean / sum
    weights: {
        distance: 0.5,
        coverage: 0.5
    }
},
config =  _.merge(defaultConfig, require("../../../config/config").colorAnalysis); // Import configuration

// Export
module.exports = (function() {

    // ------------------- Public API ------------------
    return {

         // Prepare the data for processing by this comparator
        preProcess: function(freqMap) {
            return _.map(freqMap, function(color) {
                // CIE
                color.x = (0.49     * color.r + 0.31    * color.g + 0.2     * color.b) / 0.17697;
                color.y = (0.17697  * color.r + 0.8124  * color.g + 0.01063 * color.b) / 0.17697;
                color.z = (0        * color.r + 0.01    * color.g + 0.99    * color.b) / 0.17697;
                return color;
            });
        },

        distance: {
            sum: function(color1, color2) {
                return Math.abs(color1.x - color2.x) + Math.abs(color1.y - color2.y) + Math.abs(color1.z - color2.z);
            },

            euclidean: function(color1, color2) {
                return Math.sqrt(Math.pow(color1.x - color2.x, 2) + Math.pow(color1.y - color2.y, 2) + Math.pow(color1.z - color2.z, 2));
            }
        }
    };
} ());