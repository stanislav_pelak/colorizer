module.exports = {
    rgb: require("./rgbComparator"),
    yuv: require("./yuvComparator"),
    cie: require("./cieComparator")
};