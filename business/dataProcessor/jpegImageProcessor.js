/*
 * Module responsible for JPEG images processing
 * (downsampling + color frequency analysis).
 */

// Dependencies
var _ = require("lodash"),
    jpeg = require("jpeg-js"),
    fm = require("../../model/freqMap"),
    defaultConfig = { downsamplingFactor: 32 },
    config =  _.merge(defaultConfig, require("../../config/config").imagePreprocessing);


// Business logic
module.exports = (function() {
    var JPEGImageProcessor, toExport, getPixels, downsample, countColors, fact, corr;

    //------------------- Configuration -------------------

    JPEGImageProcessor = { supportedMimetypes: ["image/jpeg"] }; // General properties

    fact = config.downsamplingFactor;
    corr = Math.round(fact / 2); // Correction - resample all colors from an interval to the center of the interval

    //------------------- Private --------------------

    // Retrieves information about pixels from the binary data
    getPixels = function(data) {
        return jpeg.decode(data).data;
    };

    // Reduces colors
    downsample = function(n) {
        // 0 - fact/2 ->
        var sample = Math.round(n / fact) * fact;
        return (sample === 256)? sample - 1 : sample;
    };

    // Count colors' occurences
    countColors = function(pixels) {
        var i, len, r, g, b, freqMap = fm.FreqMap(); //freq = {};

        for(i = 0, len = pixels.length; i < len; i += 4) {
            freqMap.addPixel(
                downsample(pixels[i]),      // r
                downsample(pixels[i + 1]),  // g
                downsample(pixels[i + 2])   // b
            );
        }
        freqMap.totalSize = len / 4; // rgba per pixel
        return freqMap;
    };

    //-------------------- Public --------------------

    // Process image data
    JPEGImageProcessor.processImage = function processImage(data) {
        return countColors(getPixels(data));
    };

    // Return an object for registering the processor
    toExport = {};
    _.forEach(JPEGImageProcessor.supportedMimetypes, function(mimeType) { toExport[mimeType] = JPEGImageProcessor }); // Register this processor for each of its supported mime types

    return toExport;
} ());