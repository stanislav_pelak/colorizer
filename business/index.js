// Dependencies
var _ = require("lodash"),
    colorAnalyzer = require("./analyzer/colorAnalyzer");

// Registered processors
var dataProcessors = {};

/* Register processors - a processor must export an object matching the following pattern:
 {
    "supported/mimetype" : ProcessorObject,  // required
    "supported/mimetype2" : ProcessorObject, // optional
    ...
 }
*/
dataProcessors = _.merge(dataProcessors, require("./dataProcessor/jpegImageProcessor"));

// Export
module.exports = {
    dataProcessors: dataProcessors,
    colorAnalyzer: colorAnalyzer
};