var _ = require("lodash");

module.exports = {

    // Sort colors array
    sortColors: function(colors) {
        if(!colors) { return []; };

        return _.sortBy(colors, function(color) {
            return -color.cnt;
        });
    },

    /**
     * Merge two colors to a new one
     * New color's properties are calculated as a weighted mean
     * of the color1/2 properties with cnt used as the weight.
     *
     * cnt and coverage properties will be sum of both colors' cnt/
     * coverage properties.
     */
    mergeColors: function(color1, color2) {
        var result, cnt1, cnt2, sum;

        if(!color1) { return color2; }
        if(!color2) { return color1; }

        cnt1 = color1.cnt;
        cnt2 = color2.cnt;
        sum = cnt1 + cnt2;

        result = {};
        _.forOwn(color1, function(value, key) {
            result[key] = (value*cnt1 + color2[key]*cnt2) / sum;
        });

        result["cnt"] = sum;
        result["coverage"] = color1.coverage + color2.coverage;

        return result;
    },

    /**
     * Adds coverage propery to each color and filters out colors with too little coverage.
     *
     * @param colors - object or array of colors to process
     * @param totalSize - total number of pixels processed
     * @param minCoverage - minimum coverage of a color to be included in the result
     * @return array of filtered colors with new "coverage" property
     */
    filterNoise: function(colors, totalSize, minCoverage) {
        return _.filter(colors, function(color) {
            color.coverage = 100 * (color.cnt || 0) / totalSize;
            return !minCoverage || (color.cnt && (color.coverage >= minCoverage));
        });
    }

};