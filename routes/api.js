// Dependencies
var _ = require("lodash"),
    express = require("express"),
    router = express.Router(),
    multer = require("multer"),
    business = require("../business"),
    processors = business.dataProcessors,
    analyzer = business.colorAnalyzer;

//----------------- Routes -----------------

// API for retrieving supported mimetypes
router.get("/types", function(req, res) {
    res.send(Object.keys(processors));
});

// API for retrieving colors' occurences in a picture
router.post("/process", multer(
    {
        // Keep the file in memory
        inMemory: true,

        // Before upload - Validate
        onFileUploadStart: function (file, req, res) {
            // MimeType
            if(processors[file.mimetype] === undefined) {
                res.status(400).send("Received mimetype is not supported: " + file.mimetype + ". Use one of: [" + Object.keys(processors) + "]");
                return false;
            }
        },

        // Process
        onFileUploadComplete: function (file, req, res) {
            var result, frequencies,
                processor = processors[file.mimetype],
                start = Date.now();

            // Process the file
            console.log("-------------------------------------------\nFile " + file.originalname + " received for processing...");
            frequencies = processor.processImage(file.buffer);
            console.log("Processing file " + file.originalname + ": part 1/2 finished (" + ((Date.now() - start) / 1000) + " s)");

            start = Date.now();
            result = analyzer.getDominantColors(frequencies);
            console.log("Processing file " + file.originalname + ": part 2/2 finished (" + ((Date.now() - start) / 1000) + " s)");

            // Response
            res.send(result);
        }
    }
));

// Return
module.exports = router;