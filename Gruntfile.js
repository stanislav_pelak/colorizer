module.exports = function(grunt) {

    grunt.initConfig({

        // (Re)Start the server
        express: {
            dev: {
                options: {
                    script: 'server.js'
                }
            }
        },

        // Minify and concatenate JS files
        uglify: {
            js: {
                files: {
                    'client/min/script.js': [

                        // Files that need to be concatenated first and in the right order
                        'client/js/index.js',
                        'client/js/i18n/*.js',

                        // Then all the rest
                        'client/js/**/*.js'
                    ]
                }
            },
            js_lib: {
                files: {
                    'client/min/lib.js': [

                        // Files that need to be concatenated first and in the right order
                        'client/lib/js/jquery-1.11.2.min.js',
                        'client/lib/js/knockout-3.3.0.js',
                        'client/lib/js/*.js',

                        // Then all the rest
                        'client/lib/js/**/*.js'
                    ]
                }
            }
        },

        // Minify and concatenate CSS files
        cssmin: {
            css: {
                files: {
                    'client/min/style.css': [

                        // First everything that needs to be first
                        'client/css/components/**/*.css',
                        'client/css/partials/**/*.css',

                        // Then all the rest
                        'client/css/**/*.css'
                    ]
                }
            },
            css_lib: {
                files: {
                    'client/min/lib.css': [

                        // First everything that needs to be first
                        'client/lib/css/*.css',

                        // Then all the rest
                        'client/lib/css/**/*.css'
                    ]
                }
            }
        },

        watch: {
            // Reload server when server sources changed
            express: {
                files: [
                    "**/*.js",
                    "**/*.json",
                    "!test/**",
                    "!client/**",
                    "!node_modules/**"
                ],
                tasks: [ "express" ],
                options: {
                    spawn: false
                }
            },

            // Run tests when any server sources or tests have changed
            tests: {
                files: [
                    "**/*.js",
                    "!client/**",
                    "!node_modules/**"
                ],
                tasks: [ "mochaTest" ],
                options: {
                    interrupt: true
                }
            },

            js: {
                files: [
                    "client/js/**/*.js"
                ],
                tasks: ["uglify:js"],
                options: {
                    interrupt: true
                }
            },

            js_lib: {
                files: [
                    "client/lib/js/**/*.js"
                ],
                tasks: ["uglify:js_lib"],
                options: {
                    interrupt: true
                }
            },

            css: {
                files: [
                    "client/css/**/*.css"
                ],
                tasks: ["cssmin:css"],
                options: {
                    interrupt: true
                }
            },

            css_lib: {
                files: [
                    "client/lib/css/**/*.css"
                ],
                tasks: ["cssmin:css_lib"],
                options: {
                    interrupt: true
                }
            }
        },

        // Run tests
        mochaTest: {
            test: {
                options: {
                    reporter: "spec"
                },
                src: ["test/**/*.js"]
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-mocha-test');

    grunt.registerTask("default", [ "mochaTest", "express", "watch" ]);
    grunt.registerTask("build_all", [ "uglify", "cssmin", "default" ]);

};