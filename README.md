# Colorizer #

Demo application for detecting representative colours in images.

### Requirements ###

* Node.js

### Start up ###

* To install the dependencies, navigate colorizer folder and run
```
#!shell

npm install
```

* To launch the application, run
```
#!shell

grunt
```


### Configuration ###

The color analysis is configurable by config/config.json file:

+ **server**
    + **port**
        * Port number to be used.
+ **imagePreprocessing**
    + **downsamplingFactor**
        * Reduce number of red, green and blue shades to be used in the analysis by this factor.
+ **colorAnalysis**
    + **minCoverage**
        * Ignore colours with smaller coverage than this number (percent).
    + **distance**
        * Distance method to be used for calculating colours' similarity (sum/euclidean).
    + **colorSpace**
        * Colour space to be used for the analysis (rgb/cie/yuv).
    + **maxDistanceToMerge**
        * Maximum distance of two colours to be considered as similar.
    + **minDistance**
        * Minimum distance of two colours to be considered the same.
    + **minResultSetSize**
        * Minimum size of the result set (preferred) - prevents "similar" colours from merging if there are not enough colours.
    + **weights**
        + **y**
            * Weight of the Y' (luminance) factor in the distance calculation (only for yuv colour space).

### Used technologies ###
+ Server-side
    + Node.js
        * Express, Lodash, Jpeg-js
        * Grunt, Mocha, Chai, Proxyquire
        * ...
+ Client-side
    * jQuery, Knockout.js, Bootstrap, Lodash, ...

### Author ###

* Stanislav Pelak, pelaksta@gmail.com


![colorizer_screen.PNG](https://bitbucket.org/repo/jyAGAM/images/3605459851-colorizer_screen.PNG)